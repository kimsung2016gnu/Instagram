package com.example.sung.instagram;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.firebase.client.Firebase;
import com.firebase.client.FirebaseApp;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;

import de.hdodenhof.circleimageview.CircleImageView;

public class SignUpActivity extends AppCompatActivity {

    private FirebaseAuth myAuth;
    private EditText userEmail,userPass,userFullName;
    private Button btnSignUp;
    private CircleImageView userProfileimg;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        myAuth = FirebaseAuth.getInstance();
        userEmail=(EditText)findViewById(R.id.txtUserName) ;
        userPass=(EditText)findViewById(R.id.txtPassword);
        userFullName=(EditText)findViewById(R.id.txtFName);
        userProfileimg=(CircleImageView)findViewById(R.id.profile_image);

        btnSignUp=(Button)findViewById(R.id.btnSignUp);
        FirebaseAuth.AuthStateListener listener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
               if (firebaseAuth.getCurrentUser() != null) {
                   FirebaseUser user = firebaseAuth.getCurrentUser();

                   FirebaseDatabase database = FirebaseDatabase.getInstance();
                   //root/user/user_id/fullname/value
                   database.getReference().child("user").child(user.getUid()).child("fullname")
                                        .setValue(String.valueOf(userFullName.getText()));
                   FirebaseStorage storage=FirebaseStorage.getInstance();
                   Bitmap bitmap = ((BitmapDrawable)userProfileimg.getDrawable()).getBitmap();
                   ByteArrayOutputStream stream = new ByteArrayOutputStream();
                   bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                   byte[] imageInByte = stream.toByteArray();
                   storage.getReference().child("user").child(user.getUid())
                           .child("photo").putBytes(imageInByte).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                       @Override
                       public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            taskSnapshot.getDownloadUrl();
                       }
                   });

               }
            }
        };
        myAuth.addAuthStateListener(listener);

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               //
                 myAuth.createUserWithEmailAndPassword(String.valueOf(userEmail.getText()),String.valueOf(userPass.getText()));
            }
        });



    //    myAuth.signInWithEmailAndPassword()
    }
}
